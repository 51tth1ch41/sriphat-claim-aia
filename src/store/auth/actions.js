import { api } from 'boot/axios'

export const doLogin = async ({ commit, dispatch }, payload) => {
  await api.post('api/LoginPatient/login-Employee', payload).then(response => {
    const token = response.data.token
    const me = response.data
    commit('setToken', token)
    api.defaults.headers.common.Authorization = 'JWT ' + token.access
    dispatch('getMe', me)
  })
}

export const signOut = ({ commit }) => {
  api.defaults.headers.common.Authorization = ''
  commit('removeToken')
}

export const getMe = async ({ commit }, me) => {
  // await api.get('/api/v1/users/me/', token.access).then(response => {
  commit('setMe', me)
}

export const init = async ({ commit, dispatch }) => {
  const token = localStorage.getItem('token')
  const me = localStorage.getItem('me')
  if (token) {
    await commit('setToken', JSON.parse(token))
    api.defaults.headers.common.Authorization = 'JWT ' + JSON.parse(token).access
    dispatch('getMe', JSON.parse(me))
  } else {
    commit('removeToken')
  }
}
