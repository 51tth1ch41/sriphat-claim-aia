export const setToken = (state, token) => {
  state.token = token
  state.isAuthenticated = true
  window.localStorage.setItem('token', JSON.stringify(token))
}

export const removeToken = (state, token) => {
  state.token = ''
  state.isAuthenticated = false
  window.localStorage.setItem('token', '')
}

export const setMe = (state, me) => {
  window.localStorage.setItem('me', JSON.stringify(me))
  state.me = me
}
