
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    meta: { requireLogin: true },
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/admin', component: () => import('pages/admin/Index'), meta: { requireLogin: true } },
      { path: '/authenticate', component: () => import('pages/claim/Authenticate'), meta: { requireLogin: true } },
      { path: '/authenticate/:idCard/:serviceSetting/:illnessType/:policyNo/:membershipId/:accidentDate', component: () => import('pages/claim/Authenticate'), meta: { requireLogin: true } },
      { path: '/discharge', component: () => import('pages/claim/DischargeList'), meta: { requireLogin: true } },
      { path: '/discharge/:illnessType/:serviceSetting/:idcard/:transactionNo', component: () => import('pages/claim/Discharge'), meta: { requireLogin: true } },
      { path: '/dischargeList', component: () => import('pages/claim/DischargeList'), meta: { requireLogin: true } },
      { path: '/register', component: () => import('pages/claim/Register'), meta: { requireLogin: true } },
      { path: '/register/request', component: () => import('pages/claim/RegisterRequest'), meta: { requireLogin: true } },
      { path: '/register/:type/:id', component: () => import('pages/claim/Register'), meta: { requireLogin: true } },
      { path: '/pre-authorize', component: () => import('pages/claim/PreAuthorize'), meta: { requireLogin: true } },
      { path: '/admission', component: () => import('pages/claim/Admission'), meta: { requireLogin: true } },
      { path: '/discharge', component: () => import('pages/claim/Discharge'), meta: { requireLogin: true } },
      { path: '/claim-status', component: () => import('pages/claim/ClaimStatus'), meta: { requireLogin: true } }
      // { path: '/billing', component: () => import('pages/claim/Billing'), meta: { requireLogin: true } },
      // { path: '/enquiry', component: () => import('pages/claim/Enquiry'), meta: { requireLogin: true } }
    ]
  },
  {
    name: 'LoginIn',
    path: '/login',
    component: () => import('pages/Login.vue')
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
